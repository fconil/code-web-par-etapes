CREATE TABLE nike (id integer primary key autoincrement, modele text, couleur text, image text, stock integer default 0);
INSERT INTO nike (modele, couleur, image, stock) values ("Nike air force 1", "blanche", "nike_air_force_1.jpeg", 10);
INSERT INTO nike (modele, couleur, image, stock) values ("Nike MK2 Tekno", "bleue", "nike_mk2_tekno.jpeg", 5);
INSERT INTO nike (modele, couleur, image, stock) values ("Nike Air Max Metallic", "doré, argenté", "nike_air_max_metallic.jpeg", 2);
