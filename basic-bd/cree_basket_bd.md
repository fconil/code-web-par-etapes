# Créer la base à partir d'instructions SQL

```shell
$ sqlite3 basket.db
SQLite version 3.37.2 2022-01-06 13:25:41
Enter ".help" for usage hints.
sqlite> 
```

```sqlite
sqlite> .read cree_basket_bd.sql
sqlite> .tables
nike
sqlite> select * from nike;
1|Nike air force 1|blanche|nike_air_force_1.jpeg|10
2|Nike MK2 Tekno|bleue|nike_mk2_tekno.jpeg|5
3|Nike Air Max Metallic|doré, argenté|nike_air_max_metallic.jpeg|2
```
