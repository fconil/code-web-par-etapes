from sqlalchemy.ext.declarative import declarative_base

from sqlalchemy import create_engine
from sqlalchemy.orm import scoped_session, sessionmaker
from sqlalchemy import Column, Integer, String

Base = declarative_base()

template = f"""<!DOCTYPE html>
<html lang="fr">
  <head>
    <meta charset="utf-8">
    <title>Baskets Nike</title>
  </head>
  <body>
    <h1>Liste des modèle Nike disponible</h1>
    <h2>Le métier d'ingénieur</h2>
    <p>Bonjour</p>
    <ol>
       <li>BAC L</li>
       <li>BAC ES</li>
       <li>BAC S</li>
    <ol>
    <p><img src="protocole-http.png"></p>
  </body>
</html>
"""

base = """<!DOCTYPE html>
<html lang="fr">
  <head>
    <meta charset="utf-8">
    <title>Baskets Nike</title>
  </head>
  <body>
    <h1>Liste des modèle Nike disponibles</h1>"""

fin = """</body>
</html>
"""

class BasketNike(Base):
    __tablename__ = 'nike'

    id = Column(Integer, primary_key=True)
    modele = Column(String)
    couleur = Column(String)
    image = Column(String)
    stock = Column(Integer)

    def __repr__(self):
        return "{0} : modèle {1}, couleur {2}, stock {3}".format(
            self.id,
            self.modele,
            self.couleur,
            self.stock
        )

if __name__ == '__main__':
    engine = create_engine("sqlite:///basket.db", echo=False)
    DBSession = scoped_session(sessionmaker())
    DBSession.configure(bind=engine, autoflush=False, expire_on_commit=False)

    print(base)
    for b in DBSession.query(BasketNike).all():
        print(f"""<div>
        <div id="modele">{b.modele}</div>
        <div id="stock">couleur: {b.couleur}, stock: {b.stock}</div>
        <div id="image_basket"><img src="./{b.image}"></div>
        </div>
        """)

    print(fin)
