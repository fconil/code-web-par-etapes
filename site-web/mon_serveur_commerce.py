from http.server import BaseHTTPRequestHandler
import os
from urllib import parse

from sqlalchemy import create_engine
from sqlalchemy.orm import scoped_session, sessionmaker

from acces_base_de_donnees import Base, BasketNike


engine = create_engine("sqlite:///basket.db", echo=False)
DBSession = scoped_session(sessionmaker())
DBSession.configure(bind=engine, autoflush=False, expire_on_commit=False)


def pageBasket(marque):
    base = """<!DOCTYPE html>
    <html lang="fr">
    <head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="style_commerce.css">
    <title>Baskets Nike</title>
    </head>
    <body>
    <h1>Liste des modèle Nike disponibles</h1>"""
    
    fin = """</body>
    </html>"""

    message = base

    for b in DBSession.query(BasketNike).all():
        message += '<div class="fiche">'
        message += f'<div class="modele">{b.modele}</div>'
        message += f'<div class="stock">couleur: {b.couleur}, stock: {b.stock}</div>'
        message += f'<div classs="image_basket"><img src="./{b.image}"></div>'
        message += '</div>'

    message += fin

    return message


class GestionGET(BaseHTTPRequestHandler):

    def recupereFichier(self):
        self.mime_types = {
            '.jpeg': ('image/jpeg', 'rb'),
            '.jpg': ('image/jpg', 'rb'),
            '.png': ('image/png', 'rb'),
            '.html': ('text/html', 'r'),
            '.css': ('text/css', 'r'),
            '.js': ('application/x-javascript', 'r')
        }
        parsed_path = parse.urlparse(self.path)

        filebase, ext = os.path.splitext(os.path.basename(parsed_path.path))
        # TODO gerer ext vide
        filename = f'{filebase}{ext}'

        if os.path.exists(filename):
            mode = self.mime_types[ext][1]

            with open(filename, mode) as f:
                contenu = f.read()
        else:
            if 'basket' in filename:
                ext = ext if len(ext) > 0 else '.html'
                return pageBasket('Nike'), ext
            else:
                contenu = None

        return contenu, ext

    def do_GET(self):
        message, ext = self.recupereFichier()

        if message is None:
            self.send_response(404)
            self.end_headers()
        else:
            mimetype = self.mime_types[ext][0]

            self.send_response(200)
            self.send_header('Content-Type',
                            f'{mimetype}; charset=utf-8')
            self.end_headers()
            if self.mime_types[ext][1] == 'r':
                self.wfile.write(message.encode('utf-8'))
            else:
                self.wfile.write(message)


if __name__ == '__main__':
    from http.server import HTTPServer
    server = HTTPServer(('localhost', 8080), GestionGET)
    print('Starting server, use <Ctrl-C> to stop')
    server.serve_forever()
