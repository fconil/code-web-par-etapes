#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Programme basique Python.
"""

import sys

if __name__ == '__main__':
    if len(sys.argv) > 1:
        prenom = sys.argv[1]
    else:
        prenom = "inconnu"

    print("Bonjour {0}".format(prenom))

    print(f"Bonjour {prenom}")
