# Encodages

L'[encodage de caractères](https://en.wikipedia.org/wiki/Character_encoding),
c'est associer des nombres à des caractères graphiques dont les lettres
utilisées dans les différentes écritures.

Historiquement, il existe un grand nombre d'encodages et on voit régulièrement
des problèmes liés à une mauvaise utilisation ou interprétation des encodages.

Aujourd'hui, l'encodage [utf-8](https://en.wikipedia.org/wiki/UTF-8) est
largement utilisé.

Auparavant, l'encodage [latin1](https://en.wikipedia.org/wiki/ISO/IEC_8859-1)
était très utilisé. Il permettait d'avoir les caractères de base et plusieurs
caractères accentués européens.

Windows utilise historiquement un encodage similaire au latin1 mais différent
: [Windows-1252](https://en.wikipedia.org/wiki/Windows-1252).

Les pages web que l'on a vues sont encodées en **utf-8**.

Travaillant sous Linux, j'utilise plusieurs outils pour manipuler les encodages
ou identifier les problèmes.

## Fonctionnement de l'encodage utf-8

L'article [How does UTF-8 turn “😂” into “F09F9882”?](https://sethmlarson.dev/blog/utf-8) 
explique le fonctionnement de cet encodage qui utilise un nombre variable
d'octets pour représenter les caractères.

Grâce à Unicode et utf-8, on peut utiliser tous les caractères que l'on
souhaite dans une page, ce qui n'était pas possible avec les anciens encodages
comme latin1.

Prenons comme exemple, le caractère "è" associé à l'entier `232`, soit `0xe8`
en [hexadécimal](https://fr.wikipedia.org/wiki/Syst%C3%A8me_hexad%C3%A9cimal)

```python
>>> ord("è")
232
>>> hex(ord("è"))
'0xe8'
```

En latin1, les caractères sont codés sur un octet et on verra bien `0xe8` pour
le caractère "è".

```python
>>> "è".encode("latin1")
b'\xe8'
```

En utf-8, les caractères dont la valeur est supérieure ou égale à `0x80` sont
codés sur 2 octets. Une partie des octets sert à indiquer sur combien d'octets
un caractère est encodé.

```python
>>> "è".encode("utf-8")
b'\xc3\xa8'
```

C'est pourquoi quand on essaie de lire un fichier encodé en utf-8 en le
décodant comme du latin1, on obtient des caractères erronés : ouvrir
`basic_mauvais_latin1.html`.

Dans l'encodage du caractère "è" sur 2 octets, les 3 premiers bits du premier
octet sont `110`, les 2 premiers bits du 2ème octet sont `10`.

Lorsque l'on récupère les 11 autres bits qui codent le nombre associé au
caractère "è" : `00011` + `101000` => `00011101000`, on retrouve bien l'entier
232 (0xe8 en hexadécimal).

```python
>>> bin(0xc3a8)
'0b1100001110101000'
   ---     --

>>> hex(0b00011101000)
'0xe8'
```

## Convertir un fichier utf-8 en latin1

J'utilise [iconv](https://doc.ubuntu-fr.org/iconv) pour convertir notre fichier
utf-8 en latin1.

```shell
$ iconv -f utf-8 -t latin1 basic_utf8.html -o basic_latin1.html
```

## Visualiser l'encodage utilisé

Pourquoi le codage est-il différent en latin1 et en utf-8 ? À cause de la
longueur variable des caractères.

https://sethmlarson.dev/blog/utf-8

```shell
$ hexdump -C basic_utf8.html 
00000000  3c 21 44 4f 43 54 59 50  45 20 68 74 6d 6c 3e 0a  |<!DOCTYPE html>.|
00000010  3c 68 74 6d 6c 20 6c 61  6e 67 3d 22 66 72 22 3e  |<html lang="fr">|
00000020  0a 20 20 3c 68 65 61 64  3e 0a 20 20 20 20 3c 6d  |.  <head>.    <m|
00000030  65 74 61 20 63 68 61 72  73 65 74 3d 22 75 74 66  |eta charset="utf|
00000040  2d 38 22 3e 0a 20 20 20  20 3c 74 69 74 6c 65 3e  |-8">.    <title>|
00000050  53 74 61 67 65 20 33 c3  a8 6d 65 3c 2f 74 69 74  |Stage 3..me</tit|
00000060  6c 65 3e 0a 20 20 3c 2f  68 65 61 64 3e 0a 20 20  |le>.  </head>.  |
00000070  3c 62 6f 64 79 3e 0a 20  20 20 20 3c 68 31 3e 44  |<body>.    <h1>D|
00000080  c3 a9 63 6f 75 76 65 72  74 65 20 64 75 20 6c 61  |..couverte du la|
00000090  62 6f 72 61 74 6f 69 72  65 3c 2f 68 31 3e 0a 20  |boratoire</h1>. |
000000a0  20 20 20 3c 68 32 3e 4c  65 20 6d c3 a9 74 69 65  |   <h2>Le m..tie|
000000b0  72 20 64 27 69 6e 67 c3  a9 6e 69 65 75 72 3c 2f  |r d'ing..nieur</|
000000c0  68 32 3e 0a 20 20 20 20  3c 70 3e 42 6f 6e 6a 6f  |h2>.    <p>Bonjo|
000000d0  75 72 3c 2f 70 3e 0a 20  20 20 20 3c 6f 6c 3e 0a  |ur</p>.    <ol>.|
000000e0  20 20 20 20 20 20 20 3c  6c 69 3e 42 41 43 20 4c  |       <li>BAC L|
000000f0  3c 2f 6c 69 3e 0a 20 20  20 20 20 20 20 3c 6c 69  |</li>.       <li|
00000100  3e 42 41 43 20 45 53 3c  2f 6c 69 3e 0a 20 20 20  |>BAC ES</li>.   |
00000110  20 20 20 20 3c 6c 69 3e  42 41 43 20 53 3c 2f 6c  |    <li>BAC S</l|
00000120  69 3e 0a 20 20 20 20 3c  2f 6f 6c 3e 0a 20 20 3c  |i>.    </ol>.  <|
00000130  2f 62 6f 64 79 3e 0a 3c  2f 68 74 6d 6c 3e 0a 0a  |/body>.</html>..|
00000140
```

```shell
$ hexdump -C basic_latin1.html
00000000  3c 21 44 4f 43 54 59 50  45 20 68 74 6d 6c 3e 0a  |<!DOCTYPE html>.|
00000010  3c 68 74 6d 6c 20 6c 61  6e 67 3d 22 66 72 22 3e  |<html lang="fr">|
00000020  0a 20 20 3c 68 65 61 64  3e 0a 20 20 20 20 3c 6d  |.  <head>.    <m|
00000030  65 74 61 20 63 68 61 72  73 65 74 3d 22 6c 61 74  |eta charset="lat|
00000040  69 6e 2d 31 22 3e 0a 20  20 20 20 3c 6c 69 6e 6b  |in-1">.    <link|
00000050  20 72 65 6c 3d 22 73 74  79 6c 65 73 68 65 65 74  | rel="stylesheet|
00000060  22 20 68 72 65 66 3d 22  73 74 79 6c 65 2e 63 73  |" href="style.cs|
00000070  73 22 3e 0a 20 20 20 20  3c 74 69 74 6c 65 3e 53  |s">.    <title>S|
00000080  74 61 67 65 20 33 e8 6d  65 3c 2f 74 69 74 6c 65  |tage 3.me</title|
00000090  3e 0a 20 20 3c 2f 68 65  61 64 3e 0a 20 20 3c 62  |>.  </head>.  <b|
000000a0  6f 64 79 3e 0a 20 20 20  20 3c 68 31 3e 44 e9 63  |ody>.    <h1>D.c|
000000b0  6f 75 76 65 72 74 65 20  64 75 20 6c 61 62 6f 72  |ouverte du labor|
000000c0  61 74 6f 69 72 65 3c 2f  68 31 3e 0a 20 20 20 20  |atoire</h1>.    |
000000d0  3c 68 32 3e 4c 65 20 6d  e9 74 69 65 72 20 64 27  |<h2>Le m.tier d'|
000000e0  69 6e 67 e9 6e 69 65 75  72 3c 2f 68 32 3e 0a 20  |ing.nieur</h2>. |
000000f0  20 20 20 3c 70 3e 42 6f  6e 6a 6f 75 72 3c 2f 70  |   <p>Bonjour</p|
00000100  3e 0a 20 20 20 20 3c 6f  6c 3e 0a 20 20 20 20 20  |>.    <ol>.     |
00000110  20 20 3c 6c 69 3e 42 41  43 20 4c 3c 2f 6c 69 3e  |  <li>BAC L</li>|
00000120  0a 20 20 20 20 20 20 20  3c 6c 69 3e 42 41 43 20  |.       <li>BAC |
00000130  45 53 3c 2f 6c 69 3e 0a  20 20 20 20 20 20 20 3c  |ES</li>.       <|
00000140  6c 69 3e 42 41 43 20 53  3c 2f 6c 69 3e 0a 20 20  |li>BAC S</li>.  |
00000150  20 20 3c 2f 6f 6c 3e 0a  20 20 20 20 3c 68 32 3e  |  </ol>.    <h2>|
00000160  c9 63 68 61 6e 67 65 73  20 65 6e 20 70 72 6f 74  |.changes en prot|
00000170  6f 63 6f 6c 65 20 48 54  54 50 3c 2f 68 32 3e 0a  |ocole HTTP</h2>.|
00000180  20 20 20 20 3c 64 69 76  3e 3c 69 6d 67 20 73 72  |    <div><img sr|
00000190  63 3d 22 70 72 6f 74 6f  63 6f 6c 65 2d 68 74 74  |c="protocole-htt|
000001a0  70 2e 70 6e 67 22 3e 3c  2f 64 69 76 3e 0a 20 20  |p.png"></div>.  |
000001b0  3c 2f 62 6f 64 79 3e 0a  3c 2f 68 74 6d 6c 3e 0a  |</body>.</html>.|
000001c0  0a                                                |.|
000001c1
```

