# Métier des ingénieurs et techniciens

Portail des métiers au CNRS :

- [Portail des métiers IT du CNRS](http://metiersit.dsi.cnrs.fr/)
- [Métiers des ingénieurs et personnels techniques de recherche et de formation du ministère](https://data.enseignementsup-recherche.gouv.fr/pages/referens_iii/)

La bande dessinée "Les décodeuses du numérique" présente 12 portraits de
chercheuses, enseignantes-chercheuses et ingénieures dans les sciences du
numérique : [Les décodeuses du numérique](https://www.ins2i.cnrs.fr/fr/les-decodeuses-du-numerique)

Elle est accessible en ligne gratuitement.

Vous trouverez beaucoup d'informations sur le site de
l'[ONISEP](https://www.onisep.fr/). Certaines brochures sont payantes mais
beaucoup d'informations sont en ligne.

- [Quelques métiers de l'informatique par l'ONISEP](https://www.onisep.fr/recherche?context=metier&search_onisep=true&text=informatique)
- [Les métiers des mathématiques de la statistique et de l'informatique](https://www.onisep.fr/decouvrir-les-metiers/des-metiers-qui-recrutent/la-collection-zoom-sur-les-metiers/Les-metiers-des-mathematiques-de-la-statistique-et-de-l-informatique)


