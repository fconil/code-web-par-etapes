# Langage de programmation Python

## Information générales

1. [site web du langage](https://www.python.org/)
2. [Documentation du langage](https://docs.python.org/3/)

## Librairie standard

### http.server

Pour lancer un petit serveur web dans le répertoire courant :

```shell
$ python -m http.server 2000
```

- Librairie [http.server](https://docs.python.org/3/library/http.server.html)

## Requests

Librairie permettant de faire des requêtes HTTP

- [Documentation Requests](http://docs.python-requests.org/en/master/)

```python
import requests

resp = requests.get("https://en.wikipedia.org/wiki/Web_colors")
resp.status_code
resp.headers
resp.request
resp.request.headers
resp.request.body
resp.request.url
```

## SQLAlchemy

Librairie d'accès à une base de données.

- [Site web SQLAlchemy](https://www.sqlalchemy.org/)
- [Documentationi SQLAlchemy](https://docs.sqlalchemy.org/en/14/)

## Beautiful Soup

Librairie pour extraire des éléments de pages HTML.

- [Site web Beautiful Soup](https://www.crummy.com/software/BeautifulSoup/)
- [Documentation Beautiful Soup](https://www.crummy.com/software/BeautifulSoup/bs4/doc/)

## Pandas

Librairie de manipulation de données.

- [Site web Pandas](https://pandas.pydata.org/)
- [Documentation Pandas](https://pandas.pydata.org/docs/)


## matplotlib

Création de graphiques, notamment via Pandas.

- [Site web matplotlib](https://matplotlib.org/)

