# Programmation web

Présentation de la [programmation Web à une classe de 3ème](http://acordier.net/blendteens)

Protocoles :

- [ARPANET](https://fr.wikipedia.org/wiki/ARPANET)
- [pile de protocoles "TCP/IP"](]https://fr.wikipedia.org/wiki/Suite_des_protocoles_Internet)

Activer les outils de développements web dans un navigateur, "F12".

- Firefox : Outils > Développement web > Outils de développement
  Cliquer sur l'onglet "Réseau" et regarder les échanges avec les serveurs
- Chrome : Outils > Outils de développement
  Cliquer sur l'onglet "Network" et regarder les échanges avec les serveurs

Ressources (peut-être un peu datées) pour les langages HTML, CSS, Javascript

- [Resources for Developers - Mozilla](https://developer.mozilla.org/fr/)
- [Standard HTML](https://www.w3.org/TR/html52/)
- [Aggrégateur devdocs.io](http://devdocs.io/)

Démonstration des changements qui peuvent être réalisés en CSS [CSS Zen
Garden](http://www.csszengarden.com/)
