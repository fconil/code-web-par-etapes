# Accueil stagiaires

# Présentation des métiers

[Métiers](metiers.md)

# Programmation Web

[Programmation Web](programmation-web.md)

# Encodages

[Encodages](encodages.md)

# Langage Python

[Langage Python](langage-python.md)

